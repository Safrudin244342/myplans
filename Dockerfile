FROM openjdk:11-jdk

WORKDIR /android

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip
RUN unzip -d android-sdk-linux android-sdk.zip

RUN echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=/android/android-sdk-linux "platforms;android-31" >/dev/null
RUN echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=/android/android-sdk-linux "platform-tools" >/dev/null
RUN echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=/android/android-sdk-linux "build-tools;28.0.2" >/dev/null
ENV PATH=$PATH:/android/android-sdk-linux/platform-tools/
# RUN chmod +x ./gradlew
# # temporarily disable checking for EPIPE error and use yes to accept all licenses
# RUN set +o pipefail
RUN yes | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=/android/android-sdk-linux --licenses
# RUN set -o pipefail
ENV ANDROID_SDK_ROOT=/android/android-sdk-linux